import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { GithubService } from '../services/github.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page /* implements OnInit */ {

  constructor(
    public githubService: GithubService,
    // private activatedRoute: ActivatedRoute
  ) { }

}
