import { Component, OnInit, Injectable } from '@angular/core';
import { Apollo, QueryRef  } from 'apollo-angular';
import gql from 'graphql-tag';
import { Observable  } from 'rxjs';
import { map  } from 'rxjs/operators';
// import { Plugins  } from '@capacitor/core';
import { Storage  } from '@ionic/storage';

// const { Storage  } = Plugins;

const commitSubscription = gql`
subscription commitSubscription($id: String!) {
  node(id: $id) {
    ... on Repository {
      id
      name
      defaultBranchRef {
        target {
          ... on Commit {
            abbreviatedOid
            oid
            author {
              name
              avatarUrl
            }
            message
          }
        }
      }
    }
  }
}`;

interface Repo {
  owner: string;
  name: string;
  avatarUrl: string;
}

@Injectable({
  providedIn: 'root'
})
export class GithubService {
  public bookmarks: Map<string, Repo> = new Map<string, Repo>();

  constructor(
    private apollo: Apollo,
    private storage: Storage,
  ) {
    this.storage.forEach((value, key, iter) => {
      this.bookmarks.set(key, value);
    });
  }

  bookmark(owner: string, repoName: string, avatarUrl: string) {
    const key = owner + ' / ' + repoName;
    const value ={ owner: owner, name: repoName, avatarUrl: avatarUrl }
    this.bookmarks.set(key, value);
    this.storage.set(key, value);
  }

  isBookmark(owner: string, repoName: string): boolean {
    const key = owner + ' / ' + repoName;
    return this.bookmarks.has(key);
  }

  bookmarkRemove(owner: string, repoName: string) {
    const key = owner + ' / ' + repoName;
    this.storage.remove(key);
    this.bookmarks.delete(key);
  }

}
