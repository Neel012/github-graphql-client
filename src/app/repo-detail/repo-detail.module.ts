import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RepoDetailPageRoutingModule } from './repo-detail-routing.module';

import { RepoDetailPage } from './repo-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RepoDetailPageRoutingModule
  ],
  declarations: [RepoDetailPage]
})
export class RepoDetailPageModule {}
