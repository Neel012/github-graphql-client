import { Component, OnInit } from '@angular/core';
import { ActivatedRoute  } from '@angular/router';
import { Apollo, QueryRef  } from 'apollo-angular';
import gql from 'graphql-tag';
import { GithubService } from '../services/github.service';

const commitsQuery = gql`
query repo($owner: String!, $repoName: String!, $limit: Int) {
  repository(name: $repoName, owner: $owner) {
    owner {
      avatarUrl(size: 32)
    }
    defaultBranchRef {
      target {
        ... on Commit {
          id
          oid
          history(first: $limit) {
            pageInfo {
              hasNextPage
              endCursor
            }
            nodes {
              message
              author {
                avatarUrl
                name
              }
              abbreviatedOid
              committedDate
              changedFiles
              additions
              deletions
            }
          }
        }
      }
    }
  }
}`;

const commitsMoreQuery = gql`
query repo($owner: String!, $repoName: String!, $limit: Int, $cursor: String) {
  repository(name: $repoName, owner: $owner) {
    owner {
      login
      avatarUrl(size: 32)
    }
    defaultBranchRef {
      target {
        ... on Commit {
          id
          oid
          history(first: $limit, after: $cursor) {
            pageInfo {
              hasNextPage
              endCursor
            }
            nodes {
              message
              author {
                avatarUrl
                name
              }
              abbreviatedOid
              committedDate
              changedFiles
              additions
              deletions
            }
          }
        }
      }
    }
  }
}`;


@Component({
  selector: 'app-repo-detail',
  templateUrl: './repo-detail.page.html',
  styleUrls: ['./repo-detail.page.scss'],
})
export class RepoDetailPage implements OnInit {
  public owner = "";
  public repoName = "";
  public commits = [];
  public avatarUrl: string = "";

  commitsQuery: QueryRef<any>;
  cursor: any;

  constructor(
    private apollo: Apollo,
    private activatedRoute: ActivatedRoute,
    public githubService: GithubService,
  ) { }

  bookmark() {
    if (this.githubService.isBookmark(this.owner, this.repoName)) {
      this.githubService.bookmarkRemove(this.owner, this.repoName);
    } else {
      this.githubService.bookmark(this.owner, this.repoName, this.avatarUrl);
    }
  }

  ngOnInit() {
    this.owner = this.activatedRoute.snapshot.paramMap.get('owner');
    this.repoName = this.activatedRoute.snapshot.paramMap.get('name');
    this.commitsQuery = this.apollo.watchQuery<any[]>({
      query: commitsQuery,
      variables: {
        limit: 25,
        owner: this.owner,
        repoName: this.repoName,
      },
    });
    this.commitsQuery
      .valueChanges
      .subscribe(({data}) => {
        this.commits = data.repository.defaultBranchRef.target.history.nodes;
        this.cursor = data.repository.defaultBranchRef.target.history.pageInfo.endCursor;
        this.avatarUrl = data.repository.owner.avatarUrl;
    });
  }

  commitsMore(event): void {
    setTimeout(() => {
      this.commitsQuery.fetchMore({
        query: commitsMoreQuery,
        variables: {
          limit: 25,
          owner: this.owner,
          repoName: this.repoName,
          cursor: this.cursor,
        },
        updateQuery: (prev, { fetchMoreResult }) => {
          this.commits.push(...fetchMoreResult.repository.defaultBranchRef.target.history.nodes);
          this.cursor = fetchMoreResult.repository.defaultBranchRef.target.history.pageInfo.endCursor;
          event.target.complete();
          if (!fetchMoreResult.repository.defaultBranchRef.target.history.pageInfo.hasNextPage) {
            // disable infinite scroll
            event.target.disabled = true;
          }
          // return [...prev, ...fetchMoreResult.search.edges]
          // return Object.assign({}, prev, fetchMoreResult);
          // return {
          //   search: {
          //     edges: [...prev.search.edges, ...fetchMoreResult.search.edges]
          //   }
          // };
          return prev;
        },
      });
    }, 500);
  }

}
