import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RepoDetailPage } from './repo-detail.page';

const routes: Routes = [
  {
    path: ':owner/:name',
    component: RepoDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RepoDetailPageRoutingModule {}
