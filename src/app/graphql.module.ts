import { NgModule } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { ApolloModule, APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
// import { WebSocketLink } from 'apollo-link-ws';

const uri = 'https://api.github.com/graphql'; // <-- add the URL of the GraphQL server here
const token = 'ee663e5d982161a97ee14422c740c49206992d9e';

// const wsClient = new WebSocketLink({
//   uri: `ws://localhost:5000/`,
//   options: {
//     reconnect: true,
//   },
// });

export function createApollo(httpLink: HttpLink) {
  return {
    link: httpLink.create({
      uri: uri,
      headers: new HttpHeaders({
        authorization: "Bearer " + token
      })
    }),
    cache: new InMemoryCache(),
  };
}

@NgModule({
  exports: [ApolloModule, HttpLinkModule],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink],
    },
  ],
})
export class GraphQLModule {}
