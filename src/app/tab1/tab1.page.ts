import { Component, OnInit, Injectable, ViewChild } from '@angular/core';
import { IonInfiniteScroll  } from '@ionic/angular';
import { Observable  } from 'rxjs';
import { Apollo, QueryRef  } from 'apollo-angular';
import gql from 'graphql-tag';
import { GithubService } from '../services/github.service';

const searchQuery = gql`query searchQuery($searchedRepo: String!, $limit: Int) {
  search(query: $searchedRepo, type: REPOSITORY, first: $limit) {
    edges {
      cursor
      node {
        ... on Repository {
          name
          owner {
            login
            avatarUrl(size: 32)
          }
        }
      }
    }
    pageInfo {
      hasNextPage
    }
  }
}`;

const searchQueryMore = gql`
query searchQueryMore($searchedRepo: String!, $limit: Int, $cursor: String) {
  search(query: $searchedRepo, type: REPOSITORY, first: $limit, after: $cursor) {
    edges {
      cursor
      node {
        ... on Repository {
          name
          owner {
            login
            avatarUrl(size: 32)
          }
        }
      }
    }
    pageInfo {
      hasNextPage
    }
  }
}`;


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  searchTerm: string;
  public repos: any[] = [];
  // public repos: Observable<any>;
  searchQuery: QueryRef<any>;

  constructor(
    private apollo: Apollo,
    public githubService: GithubService,
  ) { }

  searchRepo(event) {
    // const searchTerm = event.srcElement.value;
    if (!this.searchTerm) {
      return;
    }
    this.searchQuery = this.apollo.watchQuery<any[]>({
      query: searchQuery,
      variables: {
        limit: 25,
        searchedRepo: this.searchTerm,
      },
    });
    this.searchQuery
      .valueChanges
      .subscribe(({data}) => {
        // this.cursor = data.search.edges[data.search.edges.length - 1].cursor;
        this.repos = data.search.edges;
    });
  }

  searchRepoMore(event) {
    setTimeout(() => {
      this.searchQuery.fetchMore({
        query: searchQueryMore,
        variables: {
          limit: 25,
          searchedRepo: this.searchTerm,
          cursor: this.repos[this.repos.length - 1].cursor,
        },
        updateQuery: (prev, { fetchMoreResult }) => {
          this.repos.push(...fetchMoreResult.search.edges);
          event.target.complete();
          if (!fetchMoreResult.search.pageInfo.hasNextPage) {
            // disable infinite scroll
            event.target.disabled = true;
          }
          // return [...prev, ...fetchMoreResult.search.edges]
          // return Object.assign({}, prev, fetchMoreResult);
          // return {
          //   search: {
          //     edges: [...prev.search.edges, ...fetchMoreResult.search.edges]
          //   }
          // };
          return prev;
        },
      });
      // event.target.complete();
    }, 500);
  }

}
